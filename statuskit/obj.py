from typing import NamedTuple

Trigger = NamedTuple('Trigger', [
    ('hostid', int),
    ('triggerid', int),
    ('description', str),
    ('priority', int),
    ('componentid', int)
])
