import pprint
import time
import threading
pp = pprint.PrettyPrinter(compact=True, indent=2)
from typing import List

import requests

from .obj import Trigger
from .zabbix import Zabbix
from ._logger import logger


class StatusKit:

    def __init__(self, zabbix_user: str, zabbix_password: str, zabbix_url: str, statuskit_url: str) -> None:
        self.statuskit_url = statuskit_url
        self.zbx = Zabbix(zabbix_url, zabbix_user, zabbix_password)
        hosts_from_statuskit: List[dict] = [
            {
                'hostname': item.get('hostname'),
                'id': item.get('id')
            } for item in requests.get(statuskit_url).json()]
        hosts: List[dict] = []
        self.events_map = dict()
        self.incidents: dict = dict()
        for host in hosts_from_statuskit:
            host_zabbix = self.zbx.host_by_name(host.get('hostname')).get('result')[-1]
            host_zabbix['componentid'] = host.get('id')
            logger.debug('[%s][%s]' % (host_zabbix.get('hostid'), host_zabbix.get('host')))
            hosts.append(host_zabbix)
        self.triggers: List[Trigger] = []
        for host in hosts:
            raw_triggers = self.zbx.trigger_by_hostid(host.get('hostid')).get('result')
            for trigger in raw_triggers:
                self.triggers.append(
                    Trigger(
                        hostid=int(host.get('hostid')),
                        triggerid=int(trigger.get('triggerid')),
                        description=trigger.get('description'),
                        priority=int(trigger.get('priority')),
                        componentid=int(host.get('componentid'))
                    ))
                logger.debug('[%s][%s][%s]' % (trigger.get('priority'), trigger.get('triggerid'), trigger.get('description')))
        logger.info("monitoring '%d' hosts on Zabbix and '%d' Triggers." % (len(hosts), len(self.triggers)))

    def run(self):
        while True:
            for trigger in self.triggers:
                self.get_events(trigger)
            time.sleep(1)

    def run_mutithread(self):
        threads = list()
        while True:
            for trigger in self.triggers:
                event = threading.Thread(
                    target=self.get_events, args=(trigger,))
                threads.append(event)
                event.start()
            for th in threads:
                th.join()
            logger.info('end threads...')

    def run_by_triggers(self):
        while True:
            self.get_events_by_triggers(self.triggers)
            time.sleep(1)

    def get_events_by_triggers(self, triggers: List[Trigger]):
        raw_events = self.zbx.events_by_triggerids(triggers).get('result')
        if not raw_events:
            return

        for event in raw_events:
            if (
                event.get('objectid') not in self.incidents
            ) and (
                int(event.get('value')) == 1
            ):
                # req = requests.get(self.statuskit_url + str(trigger.componentid))
                # component = req.json()
                # evento ainda não identificado, deverá ser cadastrado e notificado.
                pp.pprint(event)

    def get_events(self, trigger: Trigger):
        raw_events = self.zbx.event_by_triggerid(trigger.triggerid).get('result')
        if not raw_events:
            return

        logger.info("event found in trigger '%s'" % trigger.description)
        req = requests.get(self.statuskit_url + str(trigger.componentid))
        component = req.json()

        # obtém ultimo evento identificado, é necessário
        # porque no intervalo de tempo o mesmo evento pode ser
        # identificado mais de uma vez.
        event = raw_events[-1]
        if (
            event.get('objectid') in self.events_map
        ) and (
            int(event.get('value')) == 0 
        ) and (
            self.events_map[event.get('objectid')]['status'] == 'open'
        ):
            # evento já resolvido, atualizar status no Cachet.
            component['status'] = 2
            req = requests.put(self.statuskit_url + str(trigger.componentid) + '/', json=component)
            logger.info("incident on host '%s' with priority '%s' solved." % (component.get('hostname'), trigger.priority))
            # remover evento da lista global de eventos.
            self.incidents.pop(event.get('objectid'))
            self.events_map[event.get('objectid')]['status'] = 'close'
        elif (
            event.get('objectid') not in self.events_map
        ) and (
            int(event.get('value')) == 1
        ):
            # evento ainda não identificado, deverá ser cadastrado e notificado.
            self.events_map[event.get('objectid')] = {'status': 'open'}
            component['status'] = 4 if trigger.priority >= 5 else 3
            req = requests.put(self.statuskit_url + str(trigger.componentid) + '/', json=component)
            logger.info("incident on host '%s' with priority '%s'." % (component.get('hostname'), trigger.priority))
            logger.info("new incident identified on host '%s' with priority '%s'." % (component.get('hostname'), trigger.priority))
            self.incidents[event.get('objectid')] = event
