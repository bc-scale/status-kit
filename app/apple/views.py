from django.shortcuts import render, redirect
from .models import Component


def view_index(request):
    return redirect('apple')


def view_apple(request):
    raw_components = Component.objects.all()
    components = []
    contx = 1
    tmp_list = []
    for component in raw_components:
        tmp_list.append(component)
        if contx == 3:
            components.append(tmp_list)
            tmp_list, contx = [], 0
        contx += 1
    if tmp_list is not None:
        while len(tmp_list) != 3:
            tmp_list.append(Component(name='', display_name=''))
        components.append(tmp_list)

    return render(request, 'views/apple.html', {'components': components})
